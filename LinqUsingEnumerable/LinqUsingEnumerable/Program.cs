using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqUsingEnumerable
{
    class Program
    {
        static void Main(string[] args)
        {
            QueryStringWithOperators();
            QueryStringsWithEnumerableAndLambdas();
            QueryStringsWithAnonymousMethods();
            VeryComplexQueryExpression.QueryStringWithRawDelegates();

            Console.ReadLine();
        }

        static void QueryStringsWithAnonymousMethods()
        {
            Console.WriteLine("***** Using Anonymous Methods *****");

            string[] games = { "Morrowind", "Uncharted 2", "Fallout 3", "Daxter", "System Shock 2" };

            Func<string, bool> searchFilter = delegate (string game) { return game.Contains(" "); };
            Func<string, string> itemToProcess = delegate (string s) { return s; };
            var subset = games.Where(searchFilter).OrderBy(itemToProcess).Select(itemToProcess);

            foreach (string s in subset)
                Console.WriteLine(s);

            Console.WriteLine();
        }

        static void QueryStringsWithEnumerableAndLambdas()
        {
            Console.WriteLine("***** Using Enumerable / Lambda Expressions *****");

            string[] games = { "Morrowind", "Uncharted 2", "Fallout 3", "Daxter", "System Shock 2" };

            var subset = games.Where(g => g.Contains(" ")).OrderBy(g => g).Select(g => g);

            foreach (string s in subset)
                Console.WriteLine(s);

            Console.WriteLine();
        }

        static void QueryStringWithOperators()
        {
            Console.WriteLine("***** Using Query Operators *****");
            string[] games = { "Morrowind", "Uncharted 2", "Fallout 3", "Daxter", "System Shock 2" };
            var subset = from game in games where game.Contains(" ") orderby game select game;

            foreach (string s in subset)
                Console.WriteLine(s);

            Console.WriteLine();
        }
    }

    public class VeryComplexQueryExpression
    {
        public static void QueryStringWithRawDelegates()
        {
            Console.WriteLine("***** Using raw delegates *****");
            string[] games = { "Morrowind", "Uncharted 2", "Fallout 3", "Daxter", "System Shock 2" };

            Func<string, bool> searchFilter = new Func<string, bool>(Filter);
            Func<string, string> itemsToProcess = new Func<string, string>(ProcessItem);

            var subset = games.Where(searchFilter).OrderBy(itemsToProcess).Select(itemsToProcess);

            foreach (string s in subset)
                Console.WriteLine(s);

            Console.WriteLine();
        }

        public static bool Filter(string game) { return game.Contains(" "); }
        public static string ProcessItem(string game) { return game; }
    }
}
