﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SimpleSerialize
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Working with Object Serialization *****\n");

            JamesBondCar jbc = new JamesBondCar();
            jbc.canFly = true;
            jbc.canSubmerge = false;

            jbc.theRadio.stationPresets = new double[] { 89.3, 105.1, 97.1 };
            jbc.theRadio.hasTweeters = true;

            Console.WriteLine("Saving as Binary");
            SaveAsBinary(jbc, "CarData.dat");
            LoadFromBinaryFile("CarData.dat");

            Console.WriteLine("Saving as SOAP");
            SaveAsSOAP(jbc, "CarData.SOAP");
            LoadFromSoapFile("CarData.SOAP");

            Console.WriteLine("Saving as XML");
            SaveAsXML(jbc, "CarData.xml");

            SaveListOfCars();

            Console.ReadLine();
        }

        private static void SaveAsBinary(object objGraph, string fileName)
        {
            BinaryFormatter binFormatter = new BinaryFormatter();

            using (Stream fStream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                binFormatter.Serialize(fStream, objGraph);
            }
            Console.WriteLine("=> Saved car in binary format!");
        }

        private static void LoadFromBinaryFile(string fileName)
        {
            BinaryFormatter binFormatter = new BinaryFormatter();

            using (Stream fStream = File.OpenRead(fileName))
            {
                JamesBondCar carFromDisk = (JamesBondCar)binFormatter.Deserialize(fStream);
                Console.WriteLine("Can this car fly? : {0}", carFromDisk.canFly);
            }
        }

        private static void SaveAsSOAP(object objGraph, string fileName)
        {
            SoapFormatter soapFormat = new SoapFormatter();

            using (Stream fStream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                soapFormat.Serialize(fStream, objGraph);
            }

            Console.WriteLine("=> Saved car in SOAP format!");
        }

        private static void LoadFromSoapFile(string fileName)
        {
            SoapFormatter soapFormat = new SoapFormatter();

            using (Stream fStream = File.OpenRead(fileName))
            {
                JamesBondCar carFromDisk = (JamesBondCar)soapFormat.Deserialize(fStream);
                Console.WriteLine("Can this car fly? : {0}", carFromDisk.canFly);
            }
        }

        public static void SaveAsXML(object objGraph, string fileName)
        {
            // XML Serializer requires a type .. so reflect it.
            XmlSerializer xmlFormat = new XmlSerializer(objGraph.GetType());

            using (Stream fStream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                xmlFormat.Serialize(fStream, objGraph);
            }

            Console.WriteLine("=> Saved car in XML Format!");
        }

        public static void SaveListOfCars()
        {
            List<JamesBondCar> cars = new List<JamesBondCar>();
            cars.Add(new JamesBondCar(true, true));
            cars.Add(new JamesBondCar(true, false));
            cars.Add(new JamesBondCar(false, true));
            cars.Add(new JamesBondCar(false, false));

            SaveAsXML(cars, "carList.xml");
        }
    }
}

