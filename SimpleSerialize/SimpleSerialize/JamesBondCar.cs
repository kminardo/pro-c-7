﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSerialize
{
    [Serializable]
    public class JamesBondCar : Car
    {
        public JamesBondCar(bool skyWorthy, bool seaWorthy)
        {
            canFly = skyWorthy;
            canSubmerge = seaWorthy;
        }

        // XML Serializer requires a default constructor
        public JamesBondCar(){}

        public bool canFly;
        public bool canSubmerge;
    }
}
