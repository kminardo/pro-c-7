﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceNameClash
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("**** Interface name clashes ****");
            Octogon oct = new Octogon();
            ((IDrawToForm)oct).Draw();

            Console.ReadLine();
        }
    }

    class Octogon : IDrawToForm, IDrawToMemory, IDrawToPrinter
    {
        void IDrawToForm.Draw()
        {
            Console.WriteLine("Drawing to Form...");
        }

        void IDrawToMemory.Draw()
        {
            Console.WriteLine("Drawing to Memory...");
        }

        void IDrawToPrinter.Draw()
        {
            Console.WriteLine("Drawing to Printer");
        }
    }

    interface IDrawToForm
    {
        void Draw();
    }

    interface IDrawToMemory
    {
        void Draw();
    }

    interface IDrawToPrinter
    {
        void Draw();
    }
}
