﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DefaultAppDomain
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Working with the default AppDomain *****");
            InitDAD();
            DisplayDADStats();
            ListAllAssembliesInAppDomain();
            Console.ReadLine();
        }

        static void InitDAD()
        {
            AppDomain defaultAD = AppDomain.CurrentDomain;
            defaultAD.AssemblyLoad += (o, s) => { Console.WriteLine("{0} has been loaded!", s.LoadedAssembly.GetName().Name); };
        }

        static void DisplayDADStats()
        {
            AppDomain defaultAD = AppDomain.CurrentDomain;

            Console.WriteLine($"Name of this domain: {defaultAD.FriendlyName}");
            Console.WriteLine($"ID of domain in this process: {defaultAD.Id}");
            Console.WriteLine($"Is this the default domain?: {defaultAD.IsDefaultAppDomain()}");
            Console.WriteLine($"Base directory of this domain: {defaultAD.BaseDirectory}");
        }

        static void ListAllAssembliesInAppDomain()
        {
            AppDomain defaultAD = AppDomain.CurrentDomain;

            var loadedAssemblies = from a in  defaultAD.GetAssemblies() orderby a.GetName().Name select a;
            Console.WriteLine("***** Here are the assemblies loaded in {0} *****\n", defaultAD.FriendlyName);
            foreach (Assembly a in loadedAssemblies)
            {
                Console.WriteLine("-> Name: {0}", a.GetName().Name);
                Console.WriteLine("-> Version: {0}\n", a.GetName().Version);
            }
        }
    }
}
