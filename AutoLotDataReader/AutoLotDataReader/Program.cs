﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace AutoLotDataReader
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Working with Data Readers *****\n");

            using(SqlConnection connection = new SqlConnection())
            {
                var connBuilder = new SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings["AutoLot"].ConnectionString);
                connection.ConnectionString = connBuilder.ConnectionString;

                connection.Open();

                ShowConnectionStatus(connection);

                string sql = "Select * From Inventory";
                SqlCommand myCommand = new SqlCommand(sql, connection);

                using (SqlDataReader myDataReader = myCommand.ExecuteReader())
                {
                    while (myDataReader.Read())
                    {
                        Console.WriteLine($"-> Make: {myDataReader["Make"]}, PetName: {myDataReader["PetName"]}, Color: {myDataReader["Color"]}.");
                    }
                }
            }

            Console.ReadLine();
        }

        private static void ShowConnectionStatus(SqlConnection connection)
        {
            Console.WriteLine("**** Info about your connection ****");
            Console.WriteLine($"Database location: {connection.DataSource}");
            Console.WriteLine($"Database name: {connection.Database}");
            Console.WriteLine($"Timeout: {connection.ConnectionTimeout}");
            Console.WriteLine($"Connection state: {connection.State}\n");
        }
    }
}
