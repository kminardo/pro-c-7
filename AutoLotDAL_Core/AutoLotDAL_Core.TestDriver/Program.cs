﻿using System;
using System.Linq;
using AutoLotDAL_Core.DataInitalization;
using AutoLotDAL_Core.EF;
using AutoLotDAL_Core.Models;
using AutoLotDAL_Core.Repos;
using Microsoft.EntityFrameworkCore;

namespace AutoLotDAL_Core.TestDriver
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Working with EF Core 2 *****\n");
            using (var context = new AutoLotContext())
            {
                MyDataInitializer.RecreateDatabase(context);
                MyDataInitializer.InitializeDatabase(context);
                foreach (Inventory c in context.Cars)
                {
                    Console.WriteLine(c);
                }
            }
            Console.WriteLine("***** Using a Repository *****");
            using (var repo = new InventoryRepo())
            {
                foreach (Inventory c in repo.GetAll())
                {
                    Console.WriteLine(c);
                }
            }
            Console.ReadLine();
        }
    }
}
