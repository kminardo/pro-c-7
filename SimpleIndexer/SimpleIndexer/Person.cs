﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleIndexer
{
    public class Person
    {
        public Person() {}
        public Person(string first, string last, int age)
        {
            FirstName = first;
            LastName = last;
            Age = age;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }

        public override string ToString() => $"Name: {FirstName} {LastName}, Age: {Age}";
    }

    public class PersonCollection : IEnumerable
    {
        private ArrayList arPeople = new ArrayList();

        public int Count => arPeople.Count;
        public Person this[int index]
        {
            get => (Person)arPeople[index];
            set => arPeople.Insert(index, value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return arPeople.GetEnumerator();
        }
    }


    class SortPeopleByAge : IComparer<Person>
    {
        public int Compare(Person firstPerson, Person secondPerson)
        {
            if (firstPerson?.Age > secondPerson?.Age)
                return 1;
            if (firstPerson?.Age < secondPerson?.Age)
                return -1;

            return 0;
        }
    }
}
