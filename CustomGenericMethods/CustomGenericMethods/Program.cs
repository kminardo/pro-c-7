﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomGenericMethods
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Custom Generics *****");
            int a = 10, b = 90;
            Console.WriteLine($"Before swap: {a}, {b}");
            Swap<int>(ref a, ref b);
            Console.WriteLine($"After swap: {a}, {b}");
            Console.WriteLine();

            string s1 = "Hello", s2 = "There";
            Console.WriteLine($"Before swap: {a}, {b}");
            Swap<string>(ref s1, ref s2);
            Console.WriteLine($"After swap: {a}, {b}");

            DisplayBaseClass<int>();
            DisplayBaseClass<string>();

            MyGenericMethods.Swap<int>(ref a, ref b);

            Console.ReadLine();
        }

        static void Swap<T>(ref T a, ref T b)
        {
            Console.WriteLine("You sent Swap() a method of {0}", typeof(T));
            T temp = a;
            a = b;
            b = temp;
        }

        static void DisplayBaseClass<T>()
        {
            Console.WriteLine($"Base class of {typeof(T)} is {typeof(T).BaseType}");
        }
    }

    public static class MyGenericMethods
    {
        public static void Swap<T>(ref T a, ref T b)
        {
            Console.WriteLine("You sent Swap() a method of {0}", typeof(T));
            T temp = a;
            a = b;
            b = temp;
        }

        public static void DisplayBaseClass<T>()
        {
            Console.WriteLine($"Base class of {typeof(T)} is {typeof(T).BaseType}");
        }
    }
}
