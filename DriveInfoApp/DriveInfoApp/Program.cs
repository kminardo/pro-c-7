﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DriveInfoApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Working with DriveInfo *****");

            DriveInfo[] myDrives = DriveInfo.GetDrives();

            foreach (DriveInfo d in myDrives)
            {
                Console.WriteLine("Name: {0}", d.Name);
                Console.WriteLine("Type: {0}", d.DriveType);

                if (d.IsReady)
                {
                    Console.WriteLine("Free Space: {0} MB", (d.TotalFreeSpace / 1024 / 1024).ToString("N0"));
                    Console.WriteLine("Format: {0}", d.DriveFormat);
                    Console.WriteLine("Label: {0}", d.VolumeLabel);
                }

                Console.WriteLine();
            }

            Console.ReadLine();
        }
    }

    class GBFormatter : IFormatProvider
    {
        public object GetFormat(Type formatType)
        {
            throw new NotImplementedException();
        }
    }
}
