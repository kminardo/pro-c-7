﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;

namespace FunWithObservableCollections
{
    class Program
    {
        static void Main(string[] args)
        {
            Person kevin = new Person { FirstName = "Kevin", LastName = "Key", Age = 48 };
            ObservableCollection<Person> people = new ObservableCollection<Person>()
            {
                new Person {FirstName = "Peter", LastName = "Murphy", Age=52},
                kevin
            };

            people.CollectionChanged += people_CollectionChanged;

            people.Add(new Person { FirstName = "Bob", LastName = "Geezer", Age = 22 });

            people.Remove(kevin);

            Console.ReadLine();
        }

        private static void people_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            Console.WriteLine($"Actoin for this event: {e.Action}");

            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
            {
                Console.WriteLine("Here are the OLD items:");
                foreach (Person p in e.OldItems)
                {
                    Console.WriteLine(p.ToString());
                }
            }

            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                Console.WriteLine("Here are the NEW items:");
                foreach (Person p in e.NewItems)
                {
                    Console.WriteLine(p.ToString());
                }
            }
        }
    }
}
