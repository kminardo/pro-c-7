﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringIndexer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Fun with Indexers *****\n");

            PersonCollection myPeople = new PersonCollection();

            myPeople["Homer"] = new Person("Homer", "Simpson", 40);
            myPeople["Marge"] = new Person("Marge", "Simpson", 38);
            //myPeople[2] = new Person("Lisa", "Simpson", 9);
            //myPeople[3] = new Person("Bart", "Simpson", 7);

            Person homer = myPeople["Homer"];
            Console.WriteLine(homer);

            PersonCollection2 myPeople2 = new PersonCollection2();

            for (int i = 0; i < 20; i++)
            {
                myPeople2[$"Homer{i}"] = new Person($"Homer{i}", "Simpson", 40);
                myPeople2[$"Marge{i}"] = new Person($"Marge{i}", "Simpson", 38);
            }

            Person homer2 = myPeople2["Homer12"];
            Console.WriteLine(homer2);

            myPeople2.RemoveAt(12);
            myPeople2.Remove("Homer3");

            foreach (Person p in myPeople2)
            {
                Console.WriteLine(p);
            }

            myPeople2[$"HomerSpecial"] = new Person($"HomerSpecial", "Simpson", 40);

            foreach (Person p in myPeople2)
            {
                Console.WriteLine(p);
            }

            Console.WriteLine(myPeople2[10]);

            Console.ReadLine();

        }
    }
}
