﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringIndexer
{
    public class Person
    {
        public Person() { }
        public Person(string first, string last, int age)
        {
            FirstName = first;
            LastName = last;
            Age = age;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }

        public override string ToString() => $"Name: {FirstName} {LastName}, Age: {Age}";
    }

    public class PersonCollection : IEnumerable
    {
        private Dictionary<string, Person> listPeople = new Dictionary<string, Person>();

        public int Count => listPeople.Count;

        public Person this[string name]
        {
            get => (Person)listPeople[name];
            set => listPeople[name] = value;
        }

        public void ClearPeople()
        {
            listPeople.Clear();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return listPeople.GetEnumerator();
        }
    }


    public class PersonCollection2 : IEnumerable
    {
        string[] keys = new string[10];
        object[] values = new object[10];

        public Person this[string key]
        {
            get
            {
                for (int i = 0; i < keys.Length; i++)
                {
                    if (key == keys[i])
                        return (Person)values[i];
                }

                return null;
            }
            set
            {
                int lowestIndex = Array.IndexOf(keys, null);

                if (lowestIndex == -1)
                {
                    lowestIndex = keys.Length;
                    Array.Resize(ref keys, keys.Length + 10);
                    Array.Resize(ref values, values.Length + 10);
                }

                keys[lowestIndex] = key;
                values[lowestIndex] = value;
            }
        }

        public Person this[int index]
        {
            get
            {
                return (Person)values[index];
            }
        }

        public void Remove(string key)
        {
            for (int i = 0; i < keys.Length; i++)
            {
                if (key == keys[i])
                {
                    keys[i] = null;
                    values[i] = null;
                }
            }
        }

        public void RemoveAt(int index)
        {
            keys[index] = null;
            values[index] = null;
        }

        public void Clear()
        {
            keys = new string[10];
            values = new object[10];
        }

        public int Count => keys.Length;

        IEnumerator IEnumerable.GetEnumerator()
        {
            return values.GetEnumerator();
        }
    }


    class SortPeopleByAge : IComparer<Person>
    {
        public int Compare(Person firstPerson, Person secondPerson)
        {
            if (firstPerson?.Age > secondPerson?.Age)
                return 1;
            if (firstPerson?.Age < secondPerson?.Age)
                return -1;

            return 0;
        }
    }
}
