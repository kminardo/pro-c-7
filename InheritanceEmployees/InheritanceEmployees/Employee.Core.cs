﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employees
{
    abstract partial class Employee
    {
        public class BenefitPackage
        {
            public enum BenefitPackageLevel { Standard, Gold, Platinum }
            public double ComputePayDeduction()
            {
                return 125.0;
            }
        }

        // Field Data
        private string empName;
        private int empAge;
        private int empID;
        private float currPay;
        private string empSSN;
        protected BenefitPackage empBenefits = new BenefitPackage();

        // Constructors
        public Employee() { }

        public Employee(string name, int id, float pay)
            : this(name, 0, id, pay, "") { }

        public Employee(string name, int age, int id, float pay, string ssn)
            : this(name, age, id, pay)
        {
            empSSN = ssn;
        }

        public Employee(string name, int age, int id, float pay)
        {
            Name = name;
            Age = age;
            ID = id;
            Pay = pay;
        }

    }
}
