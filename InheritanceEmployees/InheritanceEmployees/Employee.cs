﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employees
{
    abstract partial class Employee
    {
        // Methods
        public virtual void GiveBonus(float amount)
        {
            Pay += amount;
        }

        public virtual void DisplayStats()
        {
            Console.WriteLine($"Name: {empName}");
            Console.WriteLine($"Age: {empAge}");
            Console.WriteLine($"ID: {empID}");
            Console.WriteLine($"Pay: {currPay}");
            Console.WriteLine($"SSN: {empSSN}");
        }

        public double GetBenefitCost()
        {
            return empBenefits.ComputePayDeduction();
        }

        // Properties
        public string Name
        {
            get { return empName; }
            set
            {
                if (value.Length > 15)
                    Console.WriteLine("Error! Nam length exceeds 15 characters!");
                else
                    empName = value;
            }
        }

        public int Age
        {
            get => empAge;
            set => empAge = value;
        }
        
        public int ID
        {
            get { return empID; }
            set { empID = value; }
        }
        
        public float Pay
        {
            get { return currPay; }
            set { currPay = value; }
        }

        public string SocialSecurityNumber
        {
            get { return empSSN; }
        }

        public BenefitPackage Benefits { get { return empBenefits; } set { empBenefits = value; } }
    }
}
