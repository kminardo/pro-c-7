﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics;

namespace ProcessManipulator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Working with Processes *****");
            ListAllRunningProcesses();

            Console.WriteLine("PID: ");
            string pIDInput = Console.ReadLine();
            int pID = int.Parse(pIDInput);
            EnumThreadsForPid(pID);
            EnumModsForPid(pID);
            StartAndKillProcesses();

            Console.ReadLine();
        }

        static void StartAndKillProcesses()
        {
            Process cProc = null;

            // Launch chrome and go to facebook!
            try
            {
                //cProc = Process.Start("chrome.exe", "www.facebook.com");
                ProcessStartInfo startInfo = new ProcessStartInfo("chrome.exe", "www.facebook.com");
                startInfo.WindowStyle = ProcessWindowStyle.Maximized;
                cProc = Process.Start(startInfo);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine("--> Press enter to kill {0}", cProc.ProcessName);
            Console.ReadLine();

            try
            {
                cProc.Kill();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static void EnumModsForPid(int pID)
        {
            Process theProc = null;
            try
            {
                theProc = Process.GetProcessById(pID);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }

            Console.WriteLine($"Here are the loaded modules for: {theProc.ProcessName}");
            ProcessModuleCollection theMods = theProc.Modules;
            foreach (ProcessModule pm in theMods)
            {
                string info = $"-> Mod Name:{pm.ModuleName}";
                Console.WriteLine(info);
            }

            Console.WriteLine("*************************\n");
        }

        static void EnumThreadsForPid(int pID)
        {
            Process theProc = null;
            try
            {
                theProc = Process.GetProcessById(pID);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }

            Console.WriteLine($"Threads used by {theProc.ProcessName}");
            ProcessThreadCollection theThreads = theProc.Threads;

            foreach (ProcessThread pt in theThreads)
            {
                string info = $"ID:{pt.Id}\tStartTime:{pt.StartTime.ToShortTimeString()}\tPriority:{pt.PriorityLevel}";
                Console.WriteLine(info);
            }

            Console.WriteLine("*************************\n");
        }

        static void GetSpecificProcess(int procId)
        {
            Process theProc = null;
            try
            {
                theProc = Process.GetProcessById(procId);
                string info = $"-> PID: {theProc.Id}\tName: {theProc.ProcessName}";
                Console.WriteLine(info);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        static void ListAllRunningProcesses()
        {
            var runningProcs = from proc in Process.GetProcesses(".") orderby proc.Id select proc;

            foreach (var p in runningProcs)
            {
                string info = $"-> PID: {p.Id}\tName: {p.ProcessName}";
                Console.WriteLine(info);
            }

            int manipulatorId = (from i in runningProcs where i.ProcessName == "ProcessManipulator" select i.Id).First();
            Console.WriteLine("Process Manipulator @ ID: {0}", manipulatorId);

            Console.WriteLine("*************************\n");
        }
    }
}
