﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoProperties
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Fun with Automatic Properties *****");

            Car c = new Car();
            c.PetName = "Frank";
            c.Speed = 55;
            c.Color = "Red";
            c.DisplayStats();

            Garage g = new Garage();
            g.MyAuto = c;
            Console.WriteLine($"Number of cars: {g.NumberOfCars}");
            Console.WriteLine($"Your car is named: {g.MyAuto.PetName}");

            Console.ReadLine();
        }
    }
}
