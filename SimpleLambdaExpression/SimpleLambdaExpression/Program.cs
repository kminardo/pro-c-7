﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleLambdaExpression
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> test = new List<string> { "This", "Is", "Test" };
            TraditionalDelegateSyntax();
            AnonymousSyntax();
            LambdaExpressionSyntax();

            Console.ReadLine();
        }

        static void LambdaExpressionSyntax()
        {
            List<int> list = new List<int> { 20, 1, 4, 8, 9, 44 };

            //List<int> evenNumbers = list.FindAll(i => (i % 2) == 0);

            List<int> evenNumbers = list.FindAll(i => {
                Console.WriteLine("Value of i is {0}", i);
                bool isEven = ((i % 2) == 0);
                return isEven;
            });

            foreach (int en in evenNumbers)
            {
                Console.WriteLine($"{en}");
            }
            Console.WriteLine();
        }

        static void AnonymousSyntax()
        {
            List<int> list = new List<int> { 20, 1, 4, 8, 9, 44 };

            List<int> evenNumbers = list.FindAll(delegate (int i) { return (i % 2) == 0; });

            foreach (int en in evenNumbers)
            {
                Console.WriteLine($"{en}");
            }
            Console.WriteLine();

        }

        static void TraditionalDelegateSyntax()
        {
            List<int> list = new List<int> { 20, 1, 4, 8, 9, 44 };

            Predicate<int> callback = IsNumberEven;
            List<int> evenNumbers = list.FindAll(callback);
            foreach (int en in evenNumbers)
            {
                Console.WriteLine($"{en}");
            }
            Console.WriteLine();
        }

        static bool IsNumberEven(int i)
        {
            return (i % 2) == 0;
        }
    }
}
