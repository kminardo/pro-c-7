﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomEnumeratorWithYield
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Yield Keyword *****\n");
            Garage carlot = new Garage();

            foreach (Car c in carlot)
            {
                Console.WriteLine($"{c.PetName} is going {c.CurrentSpeed} MPH");
            }

            Console.WriteLine();

            foreach (Car c in carlot.GetTheCars(true))
            {
                Console.WriteLine($"{c.PetName} is going {c.CurrentSpeed} MPH");
            }

            Console.ReadLine();

        }
    }
}
