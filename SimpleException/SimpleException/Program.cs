﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleException
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Simple Exception Example *****");
            Console.WriteLine("=> Creating a car");
            Car myCar = new Car("Zippy", 20);
            myCar.CrankTunes(true);

            try
            {
                for (int i = 0; i < 10; i++)
                {
                    myCar.Accelerate(10);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("\n*** Error! ***");
                Console.WriteLine($"Member name: {e.TargetSite}");
                Console.WriteLine($"Class defining member: {e.TargetSite.DeclaringType}");
                Console.WriteLine($"Member type: {e.TargetSite.MemberType}");
                Console.WriteLine($"Message: {e.Message}");
                Console.WriteLine($"Source: {e.Source}");
                Console.WriteLine($"Stack: {e.StackTrace}");
                Console.WriteLine($"Help Link: {e.HelpLink}");

                foreach (DictionaryEntry de in e.Data)
                {
                    Console.WriteLine($"-> {de.Key} : {de.Value}");
                }
            }

            Console.WriteLine("*** Out of exception logic ***");

            Console.ReadLine();
        }
    }
}
