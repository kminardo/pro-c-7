﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComparableCar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Fun with Object Sorting ******");
            Car[] myAutos = new Car[5];
            myAutos[0] = new Car("Rusty", 80, 1);
            myAutos[1] = new Car("Mary", 40, 234);
            myAutos[2] = new Car("Viper", 40, 34);
            myAutos[3] = new Car("Mel", 40, 4);
            myAutos[4] = new Car("Chucky", 40, 5);

            Console.WriteLine("Unsorted cars:");
            foreach (Car c in myAutos)
                Console.WriteLine($"ID: {c.CarID}, Name: {c.PetName}");
            
            Array.Sort(myAutos);
            Console.WriteLine("Ordered:");
            foreach (Car c in myAutos)
                Console.WriteLine($"ID: {c.CarID}, Name: {c.PetName}");

            Console.WriteLine("Sort by Name:");
            Array.Sort(myAutos, new PetNameComparer());
            foreach (Car c in myAutos)
                Console.WriteLine($"ID: {c.CarID}, Name: {c.PetName}");

            Console.WriteLine("Sort by name, cleaner:");
            Array.Sort(myAutos, Car.SortByPetName);

            foreach (Car c in myAutos)
                Console.WriteLine($"ID: {c.CarID}, Name: {c.PetName}");

            Console.ReadLine();
        }
    }
}
