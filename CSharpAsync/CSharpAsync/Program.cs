﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CSharpAsync
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Working with Async ===>");
            MethodReturningVoid();
            Console.WriteLine(DoWorkAsync().Result);
            Console.WriteLine("Completed");
            Console.ReadLine();
        }

        static string DoWork()
        {
            Thread.Sleep(5000);
            return "Done with work!";
        }

        static async Task<string> DoWorkAsync()
        {
            return await Task.Run(() =>
            {
                Thread.Sleep(5000);
                return "Done with async work!";
            });
        }

        static async Task MethodReturningVoid()
        {
            await Task.Run(() => { Thread.Sleep(4000); });
            Console.WriteLine("Void method complete");
        }
    }
}
