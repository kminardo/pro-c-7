﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolymorphicShapes
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Working with Polymorphism *****\n");

            ThreeDCircle threedc = new ThreeDCircle();

            Shape[] myShapes = { new Hexagon(), new Circle(), new Hexagon("Mick"), new Circle("Beth"), new Circle("Cindy"), threedc };

            foreach (Shape s in myShapes)
            {
                s.Draw();
            }

            // calls new implementation
            threedc.Draw();
            
            // Casted to base
            ((Shape)threedc).Draw();

            Console.ReadLine();
        }
    }
}
