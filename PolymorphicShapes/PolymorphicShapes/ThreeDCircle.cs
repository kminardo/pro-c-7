﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolymorphicShapes
{
    class ThreeDCircle : Circle
    {
        new public string PetName { get; set; }

        new public void Draw()
        {
            Console.WriteLine("Drawing a 3D Circle");
        }
    }
}
