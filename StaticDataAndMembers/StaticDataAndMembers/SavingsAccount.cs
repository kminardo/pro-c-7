﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticDataAndMembers
{
    class SavingsAccount
    {
        // Instance data
        public double currBalance;

        static SavingsAccount()
        {
            Console.WriteLine("Static ctor");
            currInterestRate = 0.04;
        }

        // Static data
        public static double currInterestRate;

        public static void SetInterestRate(double newRate)
        {
            currInterestRate = newRate;
        }

        public static double GetInterestRate() { return currInterestRate; }

        public SavingsAccount(double balance)
        {
            currBalance = balance;
        }
    }
}
