﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleGC
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Garbage Collection Basics *****");

            Console.WriteLine("Estimated bytes on the heap: {0}", GC.GetTotalMemory(false));

            Console.WriteLine("This OS has {0} object generations", (GC.MaxGeneration + 1));

            Car refToCar = new Car("Zippy", 50);
            Console.WriteLine(refToCar.ToString());

            Console.WriteLine("Generation of refToCar is: {0}", GC.GetGeneration(refToCar));

            // Make a ton of objects for testing
            object[] tonOfObjects = new object[500000];

            for (int i = 0; i < 500000; i++)
                tonOfObjects[i] = new object();

            GC.Collect(0, GCCollectionMode.Forced);
            GC.WaitForPendingFinalizers();

            Console.WriteLine("Generation of refToCar is: {0}", GC.GetGeneration(refToCar));

            if (tonOfObjects[9000] != null)
            {
                Console.WriteLine("Generation of tonOfObjects[9000] is: {0}", GC.GetGeneration(tonOfObjects[9000]));
            }
            else
                Console.WriteLine("tonOfObjects[9000] is dead");

            Console.WriteLine("\nGen 0 has been swept {0} times", GC.CollectionCount(0));
            Console.WriteLine("Gen 1 has been swept {0} times", GC.CollectionCount(1));
            Console.WriteLine("Gen 2 has been swept {0} times", GC.CollectionCount(2));

            Console.ReadLine();
        }

        static void MakeACar()
        {
            Car myCar = new Car();
        }
    }
}
