﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloneablePoint
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Fun with Cloning Objects *****");

            Point p1 = new Point(50, 50);
            Point p2 = p1;

            p2.X = 0;

            Console.WriteLine(p1);
            Console.WriteLine(p2);

            Point p3 = new Point(100, 100);
            Point p4 = (Point)p3.Clone();

            p4.X = 0;

            Console.WriteLine(p3);
            Console.WriteLine(p4);

            Point p5 = new Point(100, 100, "Jane");
            Point p6 = (Point)p5.DeepClone();

            Console.WriteLine(p5);
            Console.WriteLine(p6);

            p6.desc.PetName = "My New Point";
            p6.X = 9;

            Console.WriteLine(p5);
            Console.WriteLine(p6);

            Console.ReadLine();
        }
    }

    class Point : ICloneable
    {
        public int X { get; set; }
        public int Y { get; set; }
        public PointDescription desc = new PointDescription();

        public Point(int xPos, int yPos, string petName) { X = xPos; Y = yPos; desc.PetName = petName; }
        public Point(int xPos, int yPos) { X = xPos; Y = yPos; }
        public Point(){}

        public override string ToString() => $"X = {X}, Y = {Y}, Name = {desc.PetName}, ID = {desc.PointID}";

        //public object Clone() => new Point(this.X, this.Y);

        public object Clone() => this.MemberwiseClone();

        public object DeepClone()
        {
            Point newPoint = (Point)this.MemberwiseClone();

            PointDescription currentDesc = new PointDescription();
            currentDesc.PetName = this.desc.PetName;
            newPoint.desc = currentDesc;

            return newPoint;
        }
    }

    public class PointDescription
    {
        public string PetName { get; set; }
        public Guid PointID { get; set; }

        public PointDescription()
        {
            PetName = "No-Name";
            PointID = Guid.NewGuid();
        }
    }
}
