﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Structures
{
    struct Point
    {
        public int X;
        public int Y;

        public Point(int XPos, int YPos)
        {
            X = XPos;
            Y = YPos;
        }

        public void Increment() { X++; Y++; }

        public void Decrement() { X--; Y--; }

        public void Display() { Console.WriteLine($"X={X} Y={Y}"); }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** First look at structures *****");

            Point myPoint;
            myPoint.X = 349;
            myPoint.Y = 76;
            myPoint.Display();

            myPoint.Increment();
            myPoint.Display();

            Point p1;
            p1.X = 10;
            p1.Y = 10;
            p1.Display();

            Point p2 = new Point();
            p2.Display();

            Point p3 = new Point(50, 60);
            p3.Display();

            Console.ReadLine();
        }
    }
}
