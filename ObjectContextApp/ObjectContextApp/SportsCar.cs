﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ObjectContextApp
{
    class SportsCar
    {
        public SportsCar()
        {
            Context ctx = Thread.CurrentContext;
            Console.WriteLine($"{this.ToString()} object is in context {ctx.ContextID}");
            foreach (IContextProperty itfCtxProp in ctx.ContextProperties)
            {
                Console.WriteLine($"-> Ctx Prop: {itfCtxProp.Name}");
            }
        }
    }
}
