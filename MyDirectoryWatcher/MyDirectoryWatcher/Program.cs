﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDirectoryWatcher
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** The Amazing File Watcher App *****\n");

            FileSystemWatcher watcher = new FileSystemWatcher();

            try
            {
                watcher.Path = @"WatchFolder";
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
                return;
            }

            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;

            watcher.Filter = "*.txt";
            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnChanged);
            watcher.Deleted += new FileSystemEventHandler(OnChanged);
            watcher.Renamed += (s, e) => { Console.WriteLine("File: {0} renamed to {1}", e.OldFullPath, e.FullPath); };

            watcher.EnableRaisingEvents = true;

            Console.WriteLine(@"Press 'q' to quit");
            while (Console.Read() != 'q');
        }

        static void OnChanged(object source, FileSystemEventArgs e)
        {
            Console.WriteLine("File: {0} {1}!", e.FullPath, e.ChangeType);
        }
    }
}
