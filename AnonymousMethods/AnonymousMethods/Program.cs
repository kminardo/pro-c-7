﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnonymousMethods
{
    class Program
    {
        static void Main(string[] args)
        {
            int aboutToBlowCounter = 0;

            Console.WriteLine("***** Anonymous Methods *****\n");
            Car c1 = new Car("SlugBug", 100, 10);
            c1.AboutToExplode += delegate { aboutToBlowCounter++; Console.WriteLine("Too fast!"); };

            c1.AboutToExplode += delegate (object sender, CarEventArgs e)
            {
                aboutToBlowCounter++;
                Console.WriteLine("Message from Car: {0}", e.msg);
            };

            c1.Exploded += delegate (object sender, CarEventArgs e)
            {
                Console.WriteLine("Fatal message: {0}", e.msg);
            };

            for (int i = 0; i < 6; i++)
                c1.Accelerate(20);

            Console.WriteLine(aboutToBlowCounter);

            Console.ReadLine();
        }
    }
}

