﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnonymousTypes
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Working with Anonymous Types *****");

            var myCar = new { Color = "Pink", Make = "Saab", CurrentSpeed = 35 };
            Console.WriteLine($"My car is a {myCar.Color} {myCar.Make}");

            BuildAnonType("BMW", "Black", 90);

            Console.ReadLine();
        }

        static void BuildAnonType(string make, string color, int currSp)
        {
            var car = new { Make = make, Color = color, CurrentSpeed = currSp };

            Console.WriteLine($"You have a {car.Color} {car.Make} going {car.CurrentSpeed} MPH");
            Console.WriteLine($"ToString() = {car.ToString()}");
        }
    }
}
