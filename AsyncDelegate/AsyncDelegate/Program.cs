﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncDelegate
{
    public delegate int BinaryOp(int x, int y);

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Async Delegate Invocation *****");
            Console.WriteLine("Main() Invoked on thread {0}", Thread.CurrentThread.ManagedThreadId);

            BinaryOp b = new BinaryOp(Add);
            IAsyncResult result = b.BeginInvoke(10, 10, null, null);

            while (!result.IsCompleted)
            {
                Console.WriteLine("Doing more work in Main()");
                Thread.Sleep(1000);
            }
            
            int answer = b.EndInvoke(result);
            Console.WriteLine("10 + 10 is {0}", answer);
            Console.ReadLine();
        }

        static int Add(int x, int y)
        {
            Console.WriteLine("Add() invoked on thread {0}", Thread.CurrentThread.ManagedThreadId);
            // Simulate a lengthy operation
            Thread.Sleep(5000);
            return x + y;
        }
    }
}
