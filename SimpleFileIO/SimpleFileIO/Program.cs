﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFileIO
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("****** Simple I/O with the File type ******\n");
            string[] myTasks = { "Fix bathroom sink", "Call Dave", "Call Mom and Dad", "Play Xbox" };
            File.WriteAllLines(@"tasks.txt", myTasks);

            foreach (string task in File.ReadAllLines(@"tasks.txt"))
            {
                Console.WriteLine("TODO: {0}", task);
            }

            Console.WriteLine("-> Press any button to clean up");
            Console.ReadLine();
            File.Delete(@"tasks.txt");

            Console.WriteLine("Done!");
            Console.ReadLine();
        }
    }
}
