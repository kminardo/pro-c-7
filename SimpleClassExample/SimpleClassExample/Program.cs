﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Class Types *****\n");

            MakeSomeBikes();

            Car chuck = new Car();
            chuck.PrintState();

            Car mary = new Car("Mary");
            mary.PrintState();

            Car daisy = new Car("Daisy", 75);
            daisy.PrintState();

            Car myCar = new Car();
            myCar.petName = "Henry";
            myCar.currSpeed = 10;

            for (int i = 0; i < 10; i++)
            {
                myCar.SpeedUp(5);
                myCar.PrintState();
            }

            Console.ReadLine();
        }

        static void MakeSomeBikes()
        {
            Motorcycle m1 = new Motorcycle();
            Console.WriteLine($"Name = {m1.driverName}, Intensity = {m1.driverIntensity}");
            Motorcycle m2 = new Motorcycle(name: "Tiny");
            Console.WriteLine($"Name = {m2.driverName}, Intensity = {m2.driverIntensity}");
            Motorcycle m3 = new Motorcycle(7);
            Console.WriteLine($"Name = {m3.driverName}, Intensity = {m3.driverIntensity}");
        }
    }
}
