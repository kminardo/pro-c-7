﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassExample
{
    class Motorcycle
    {
        public int driverIntensity;
        public string driverName;

        //Constructor chaining
        //public Motorcycle() { Console.WriteLine("In default ctor"); }
        //public Motorcycle(int intensity)
        //    : this(intensity, "") { Console.WriteLine("In int ctor"); }
        //public Motorcycle(string name)
        //    : this(0, name) { Console.WriteLine("In string ctor"); }

        // Updated single constructor with optional parameters
        public Motorcycle(int intensity = 0, string name = "")
        {
            Console.WriteLine("In master ctor");
            if (intensity > 10) { intensity = 10; }

            driverIntensity = intensity;
            driverName = name;

        }

        public void SetDriverName(string name)
        {
            driverName = name;
        }

        public void PopAWheely()
        {
            for (int i = 0; i < driverIntensity; i++)
            {
                Console.WriteLine("Yeeee haaaeewwww!");
            }
        }
    }
}
