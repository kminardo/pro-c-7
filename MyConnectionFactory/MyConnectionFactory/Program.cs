﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Configuration;

namespace MyConnectionFactory
{
    enum DataProvider { SqlServer, OleDb, Odbc, None }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Very Simple Connection Factory *****\n");
            string dataProviderString = ConfigurationManager.AppSettings["provider"];
            DataProvider dataProvider = DataProvider.None;

            if (!String.IsNullOrEmpty(dataProviderString) && Enum.IsDefined(typeof(DataProvider), dataProviderString))
            {
                dataProvider = (DataProvider)Enum.Parse(typeof(DataProvider), dataProviderString);
            }
            else 
            {
                Console.WriteLine("Sorry - no provider exists!");
                Console.ReadLine();
                return;
            }

            IDbConnection myConnection = GetConnection(dataProvider);
            Console.WriteLine($"Your connection is a {myConnection.GetType().Name}");
            // Do DB work
            Console.ReadLine();
        }

        static IDbConnection GetConnection(DataProvider dataProvider)
        {
            IDbConnection connection = null;
            switch (dataProvider)
            {
                case DataProvider.SqlServer:
                    connection = new SqlConnection();
                    break;
                case DataProvider.OleDb:
                    connection = new OleDbConnection();
                    break;
                case DataProvider.Odbc:
                    connection = new OdbcConnection();
                    break;
            }
            return connection;
        }
    }
}
