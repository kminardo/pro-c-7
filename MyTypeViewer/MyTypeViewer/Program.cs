﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace MyTypeViewer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Welcome to MyTypeViewer *****");
            string typeName = "";

            do
            {
                Console.WriteLine("\nEnter a type name to evaluate");
                Console.Write("or enter Q to quit:");

                typeName = Console.ReadLine();

                if (typeName.Equals("Q", StringComparison.OrdinalIgnoreCase))
                {
                    break;
                }

                try
                {
                    Type t = Type.GetType(typeName);
                    Console.WriteLine();
                    ListVariousStats(t);
                    ListFields(t);
                    ListProps(t);
                    ListMethods(t);
                    ListInterfaces(t);
                }
                catch
                {
                    Console.WriteLine("Sorry, can't find type");
                }

            } while (true);
        }

        static void ListMethods(Type t)
        {
            Console.WriteLine("***** Methods *****");
            MethodInfo[] mi = t.GetMethods();
            foreach (MethodInfo m in mi)
            {
                string retVal = m.ReturnType.FullName;
                string paramInfo = "(";
                foreach (ParameterInfo pi in m.GetParameters())
                {
                    paramInfo += string.Format($"{pi.ParameterType} {pi.Name}");
                }
                paramInfo += ")";

                Console.WriteLine($"->{retVal} {m.Name} {paramInfo}");
            }
            Console.WriteLine();
        }

        static void ListFields(Type t)
        {
            Console.WriteLine("***** Fields *****");
            var fieldNames = from f in t.GetFields() select f.Name;
            foreach (var name in fieldNames)
            {
                Console.WriteLine($"-> {name}");
            }
            Console.WriteLine();
        }

        static void ListProps(Type t)
        {
            Console.WriteLine("***** Properties *****");
            var names = from f in t.GetProperties() select f.Name;
            foreach (var name in names)
            {
                Console.WriteLine($"-> {name}");
            }
            Console.WriteLine();
        }

        static void ListInterfaces(Type t)
        {
            var ifaces = from i in t.GetInterfaces() select i;
            foreach (Type i in ifaces)
                Console.WriteLine($"->{i.Name}");
        }

        static void ListVariousStats(Type t)
        {
            Console.WriteLine("***** Various Stats *****");
            Console.WriteLine($"Base class is {t.BaseType}");
            Console.WriteLine($"Is type abstract? {t.IsAbstract}");
            Console.WriteLine($"Is type sealed? {t.IsSealed}");
            Console.WriteLine($"Is type generic? {t.IsGenericTypeDefinition}");
            Console.WriteLine($"Is type a class type? {t.IsClass}");
            Console.WriteLine();
        }
    }
}
