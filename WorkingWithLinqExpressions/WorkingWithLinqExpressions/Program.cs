using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkingWithLinqExpressions
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Working with Query Expressions *****");
            ProductInfo[] itemsInStock = new[]
            {
                new ProductInfo{ Name = "Mac's Coffee", Description = "Coffee with TEETH", NumberInStock = 24},
                new ProductInfo{ Name = "Milk Maid Milk", Description = "Milk cow's love", NumberInStock = 100},
                new ProductInfo{ Name = "Pure Silk Tofu", Description = "Bland as Possible", NumberInStock = 120},
                new ProductInfo{ Name = "Crunchy Pops", Description = "Cheezy, peppery goodness", NumberInStock = 2},
                new ProductInfo{ Name = "RipOff Water", Description = "From the tap to your wallet", NumberInStock = 100},
                new ProductInfo{ Name = "Classic Valpo Pizza", Description = "Everybody loves pizza!", NumberInStock = 38}
            };

            SelectEverything(itemsInStock);
            ListProductNames(itemsInStock);
            GetOverstock(itemsInStock);
            GetNamesAndDescriptions(itemsInStock);
            Array objs = GetProjectedSubset(itemsInStock);
            foreach (object o in objs)
                Console.WriteLine(o);

            GetCountFromQuery();
            ReverseEverything(itemsInStock);
            AlphabetizeProductNames(itemsInStock);
            DisplayDiff();
            DisplayUnion();
            DisplayConcat();
            DisplayConcatNoDupes();
            AggregateOps();

            Console.ReadLine();
        }

        static void AggregateOps()
        {
            double[] winterTemps = { 2.0, -21.3, 8, -4, 0, 8.2 };

            Console.WriteLine($"Max temp: {winterTemps.Max()}");
            Console.WriteLine($"Min temp: {winterTemps.Min()}");
            Console.WriteLine($"Average temp: {winterTemps.Average()}");
            Console.WriteLine($"Sum of all temps: {winterTemps.Sum()}");
        }

        static void DisplayConcatNoDupes()
        {
            List<string> myCars = new List<string> { "Yugo", "Aztec", "BMW" };
            List<string> yourCars = new List<string> { "BMW", "Saab", "Aztec" };

            var carConcat = (from c in myCars select c).Concat(from c2 in yourCars select c2);
            Console.WriteLine("Here is distinct records concatenated:");
            foreach (string s in carConcat.Distinct())
                Console.WriteLine(s);
        }

        static void DisplayConcat()
        {
            List<string> myCars = new List<string> { "Yugo", "Aztec", "BMW" };
            List<string> yourCars = new List<string> { "BMW", "Saab", "Aztec" };

            var carConcat = (from c in myCars select c).Concat(from c2 in yourCars select c2);
            Console.WriteLine("Here is everything concatenated:");
            foreach (string s in carConcat)
                Console.WriteLine(s);
        }

        static void DisplayUnion()
        {
            List<string> myCars = new List<string> { "Yugo", "Aztec", "BMW" };
            List<string> yourCars = new List<string> { "BMW", "Saab", "Aztec" };

            var carUnion = (from c in myCars select c).Union(from c2 in yourCars select c2);
            Console.WriteLine("Here is everything:");
            foreach (string s in carUnion)
                Console.WriteLine(s);
        }

        static void DisplayDiff()
        {
            List<string> myCars = new List<string> { "Yugo", "Aztec", "BMW" };
            List<string> yourCars = new List<string> { "BMW", "Saab", "Aztec" };

            var carDiff = (from c in myCars select c).Except(from c2 in yourCars select c2);
            Console.WriteLine("Here is what you don't have, but I do:");
            foreach (string s in carDiff)
                Console.WriteLine(s);
        }

        static void AlphabetizeProductNames(ProductInfo[] products)
        {
            Console.WriteLine("Ordered by name:");
            var subset = from p in products orderby p.Name select p;
            foreach (var p in subset)
                Console.WriteLine(p);
        }

        static void ReverseEverything(ProductInfo[] products)
        {
            Console.WriteLine("Product in reverse:");
            var allProducts = from p in products select p;
            foreach (var p in allProducts.Reverse())
                Console.WriteLine(p);
        }

        static void GetCountFromQuery()
        {
            string[] games = { "Morrowind", "Uncharted 2", "Fallout 3", "Daxter", "Fun" };
            int numb = (from g in games where g.Length > 6 select g).Count();
            Console.WriteLine($"{numb} in the linq query");
        }

        static Array GetProjectedSubset(ProductInfo[] products)
        {
            Console.WriteLine("Return Anonymous Subset:");
            var nameDesc = from p in products select new { p.Name, p.Description };
            return nameDesc.ToArray();
        }

        static void GetNamesAndDescriptions(ProductInfo[] products)
        {
            Console.WriteLine("Names and Descriptions:");
            var nameDesc = from p in products select new { p.Name, p.Description };

            foreach (var item in nameDesc)
                Console.WriteLine(item);
        }

        static void GetOverstock(ProductInfo[] products)
        {
            Console.WriteLine("Overstocked items:");
            var overstock = from p in products where p.NumberInStock > 25 select p;
            foreach (ProductInfo c in overstock)
                Console.WriteLine(c);
        }

        static void ListProductNames(ProductInfo[] products)
        {
            Console.WriteLine("Only product names:");
            var names = from p in products select p.Name;
            foreach (var n in names)
                Console.WriteLine($"Name: {n}");
        }

        static void SelectEverything(ProductInfo[] products)
        {
            Console.WriteLine("All product details:");
            var allProducts = from p in products select p;
            foreach (var prod in allProducts)
                Console.WriteLine(prod.ToString());
        }
    }
}
