using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqOverObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Working with LINQ to Objects *****");
            QueryOverStrings();
            QueryOverStringsWithExtensionMethods();
            QueryOverInts();
            ImmediateExecution();
            Console.ReadLine();
        }

        static void ImmediateExecution()
        {
            int[] numbers = { 10, 20, 30, 40, 1, 2, 3, 8 };
            int[] subsetAsIntArray = (from i in numbers where i < 10 select i).ToArray<int>();
            List<int> subsetAsListOfInts = (from i in numbers where i < 10 select i).ToList<int>();
        }

        static void QueryOverInts()
        {
            int[] numbers = { 10, 20, 30, 40, 1, 2, 3, 8 };
            var subset = from i in numbers where i < 10 select i;

            // Initial evaluation
            foreach (var i in subset)
                Console.WriteLine($"Item: {i}");

            // Changing data
            numbers[0] = 4;
            
            // Re-evaluated
            foreach (var i in subset)
                Console.WriteLine($"Item: {i}");

            ReflectOverQueryResults(subset);
        }

        static void QueryOverStrings()
        {
            string[] games = { "Morrowind", "Uncharted 2", "Fallout 3", "Daxter", "System Shock 2" };
            IEnumerable<string> subset = from g in games where g.Contains(" ") orderby g select g;

            ReflectOverQueryResults(subset);

            foreach (string s in subset)
                Console.WriteLine($"Item: {s}");
        }

        static void QueryOverStringsWithExtensionMethods()
        {
            string[] games = { "Morrowind", "Uncharted 2", "Fallout 3", "Daxter", "System Shock 2" };
            IEnumerable<string> subset = games.Where(g => g.Contains(" ")).OrderBy(g => g).Select(g => g);

            ReflectOverQueryResults(subset, "Extension Methods");

            foreach (string s in subset)
                Console.WriteLine($"Item: {s}");
        }

        static void ReflectOverQueryResults(object resultSet, string queryType = "Query Expressions")
        {
            Console.WriteLine($"***** Info about your query using {queryType} *****");
            Console.WriteLine($"resultSet is of type: {resultSet.GetType().Name}" );
            Console.WriteLine($"resultSet location: {resultSet.GetType().Assembly.GetName().Name}");
        }
    }
}
