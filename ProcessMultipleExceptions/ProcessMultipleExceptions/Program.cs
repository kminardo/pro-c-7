﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessMultipleExceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Multiple Exception Handling *****");
            Car myCar = new Car("Rusty", 90);

            try
            {
                // Arg out of range exception
                myCar.Accelerate(700);
            }
            catch (CarIsDeadException ex)
            {
                try
                {
                    // Attempt to open a file named carErrors to log
                    FileStream fs = File.Open(@"C:\carErrors.txt", FileMode.Open);
                    Console.WriteLine(ex.Message);
                }
                catch (Exception e2)
                {
                    throw new CarIsDeadException(ex.Message, e2);
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                myCar.CrankTunes(false);
            }

            Console.ReadLine();

        }
    }
}
