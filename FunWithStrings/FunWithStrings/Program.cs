﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunWithStrings
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Fun with Strings *****");
            Console.WriteLine();

            BasicStringFunctionality();
            StringConcatenation();
            EscapeChars();
            StringsAreImmutable();
            StringsAreImmutable2();
            FunWithStringBuilder();
            StringInterpolation();

            Console.ReadLine();
        }

        private static void StringInterpolation()
        {
            int age = 4;
            string name = "Soren";

            string greeting = string.Format("Hello {0} you are {1} years old", name, age);
            string greeting2 = $"\tHello {name.ToUpper()} you are {age + 1} years old";

            Console.WriteLine(greeting2);
        }

        private static void FunWithStringBuilder()
        {
            Console.WriteLine("=> Using the StringBuilder:");
            StringBuilder sb = new StringBuilder("**** Fantastic Games ****", 256);
            sb.Append("\n");
            sb.AppendLine("Half Life");
            sb.AppendLine("Morrowind");
            sb.AppendLine("Dues Ex " + "2");
            sb.AppendLine("System Shock");
            Console.WriteLine(sb.ToString());
            sb.Replace("2", "Invisible War");
            Console.WriteLine(sb.ToString());
            Console.WriteLine("sb has {0} chars", sb.Length);
            Console.WriteLine();
        }

        private static void StringsAreImmutable2()
        {
            // inspect in ILDasm
            string s2 = "My other string";
            s2 = "New string value";
        }

        private static void StringsAreImmutable()
        {
            Console.WriteLine("=> Immutable Strings:");

            // set initial value
            string s1 = "This is my string.";
            Console.WriteLine("s1 = {0}", s1);

            // upper case s1?
            string upperCase = s1.ToUpper();
            Console.WriteLine("upperCase = {0}", upperCase);

            // Nope, new string.
            Console.WriteLine("s1 = {0}", s1);
        }

        private static void EscapeChars()
        {
            Console.WriteLine("Escape Characters: \a");
            string strWithTabs = "Model\tColor\tSpeed\tPet Name \a";
            Console.WriteLine(strWithTabs);

            Console.WriteLine("Everyone loves \"Hello World\" \a");
            Console.WriteLine("C:\\MyApp\\Bin\\Debug \a");

            Console.WriteLine("All finished.\n\n\n \a");
            Console.WriteLine();

            Console.WriteLine(@"C:\MyApp\Bin\Debug");
            string longString = @"This is a very
                    very
                            very
                                    long string";
            Console.WriteLine(longString);
        }

        private static void StringConcatenation()
        {
            Console.WriteLine("=> String concatenation:");
            string s1 = "Programming The ";
            string s2 = "PsychoDrill (PTP)";
            string s3 = s1 + s2;
            Console.WriteLine(s3);
            Console.WriteLine();
        }

        private static void BasicStringFunctionality()
        {
            Console.WriteLine("=> Basic String functionality:");
            string firstName = "Freddy";
            Console.WriteLine("Value of firstName: {0}", firstName);
            Console.WriteLine("firstName has {0} characters.", firstName.Length);
            Console.WriteLine("firstName in uppercase: {0}", firstName.ToUpper());
            Console.WriteLine("firstName in lowercase: {0}", firstName.ToLower());
            Console.WriteLine("firstName contains the letter y?: {0}", firstName.Contains("y"));
            Console.WriteLine("firstname after replace: {0}", firstName.Replace("dy", ""));
            Console.WriteLine();
        }
    }
}
