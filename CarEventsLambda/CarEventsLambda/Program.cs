﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarEvents
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Working with events *****");
            Car c1 = new Car("SlugBug", 100, 10);

            c1.AboutToExplode += (sender, e) => Console.WriteLine("=> Critical Message from Car: {0}", e.message);

            c1.Exploded += (sender, e) => Console.WriteLine(e.message);

            Console.WriteLine("***** Speeding up! *****");
            for (int i = 0; i < 6; i++)
            {
                c1.Accelerate(20);
            }

            Console.WriteLine("*Speeding up!*");
            for (int i = 0; i < 6; i++)
                c1.Accelerate(20);

            Console.ReadLine();
        }

        private static void C1_AboutToExplode(string msgForCaller)
        {
            throw new NotImplementedException();
        }

        public static void CarAboutToBlow(string msg)
        {
            Console.WriteLine(msg);
        }

        public static void CarIsAlmostDoomed(string msg)
        {
            Console.WriteLine("=> Critical Message from Car: {0}", msg);
        }

        public static void CarExploded(string msg)
        {
            Console.WriteLine(msg);
        }
    }
}
