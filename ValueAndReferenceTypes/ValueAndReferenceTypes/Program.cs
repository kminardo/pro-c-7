﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValueAndReferenceTypes
{
    struct Point
    {
        public int X;
        public int Y;

        public Point(int XPos, int YPos)
        {
            X = XPos;
            Y = YPos;
        }

        public void Increment() { X++; Y++; }

        public void Decrement() { X--; Y--; }

        public void Display() { Console.WriteLine($"X={X} Y={Y}"); }
    }

    class PointRef
    {
        public int X;
        public int Y;

        public PointRef(int XPos, int YPos)
        {
            X = XPos;
            Y = YPos;
        }

        public void Increment() { X++; Y++; }

        public void Decrement() { X--; Y--; }

        public void Display() { Console.WriteLine($"X={X} Y={Y}"); }
    }

    class ShapeInfo
    {
        public string InfoString;
        public ShapeInfo(string info)
        {
            InfoString = info;
        }
    }

    struct Rectangle
    {
        public ShapeInfo RectInfo;

        public int RectTop, RectLeft, RectBottom, RectRight;

        public Rectangle(string info, int top, int left, int bottom, int right)
        {
            RectInfo = new ShapeInfo(info);

            RectTop = top; RectBottom = bottom; RectLeft = left; RectRight = right;
        }

        public void Display()
        {
            Console.WriteLine("String = {0}, Top = {1}, Bottom = {2}, Right = {3}, Left = {4}", RectInfo.InfoString, RectTop, RectBottom, RectRight, RectLeft);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            ValueTypeAssignment();
            ReferenceTypeAssignment();
            ValueTypeContainingRefType();
            Console.ReadLine();
        }

        static void ValueTypeContainingRefType()
        {
            Console.WriteLine("=> Creating r1");
            Rectangle r1 = new Rectangle("First Rect", 10, 10, 50, 50);

            Console.WriteLine("=> Assigning r2 to r1");
            Rectangle r2 = r1;

            Console.WriteLine("=> Changing r2 Values");
            r2.RectInfo.InfoString = "This is new info!";
            r2.RectBottom = 4444;

            r1.Display();
            r2.Display();
        }

        static void ValueTypeAssignment()
        {
            Console.WriteLine("Assinging value types\n");
            Point p1 = new Point(10, 10);
            Point p2 = p1;

            p1.Display();
            p2.Display();

            p1.X = 100;
            Console.WriteLine("\n=>Changed p1.X\n");
            p1.Display();
            p2.Display();
        }

        static void ReferenceTypeAssignment()
        {
            Console.WriteLine("Assigning reference types\n");
            PointRef p1 = new PointRef(10, 10);
            PointRef p2 = p1;

            p1.Display();
            p2.Display();

            p1.X = 100;
            Console.WriteLine("\n=> Changed p1.X\n");
            p1.Display();
            p2.Display();
        }
    }
}
