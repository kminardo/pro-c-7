﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MutipleInheritanceInterfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle rect = new Rectangle();
            rect.Print();
            ((IPrintable)rect).Draw();
            IDrawable drawRect = rect as IDrawable;
            drawRect.Draw();
            Console.WriteLine(rect.GetNumberOfSides());

            Console.ReadLine();
        }
    }

    interface IDrawable
    {
        void Draw();
    }

    interface IPrintable
    {
        void Print();
        void Draw();
    }

    interface IShape : IDrawable, IPrintable
    {
        int GetNumberOfSides();
    }

    class Rectangle : IShape
    {
        public int GetNumberOfSides() => 4;

        void IPrintable.Draw() => Console.WriteLine("Drawing to printer...");

        void IDrawable.Draw() => Console.WriteLine("Drawing to screen...");

        public void Print() => Console.WriteLine("Printing...");
    }
}
