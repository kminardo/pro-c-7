﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using AutoLotDAL.Models;

namespace AutoLotDAL.DataOperations
{
    public class InventoryDAL
    {
        private readonly string _connectionString;
        private SqlConnection _sqlConnection = null;

        public InventoryDAL() : this(@"Data Source = (localdb)\mssqllocaldb;Integrated Security=true;Initial Catalog=AutoLot") { }
        public InventoryDAL(string connectionString) => _connectionString = connectionString;

        private void OpenConnection()
        {
            _sqlConnection = new SqlConnection { ConnectionString = _connectionString };
            _sqlConnection.Open();
        }
        private void CloseConnection()
        {
            if (_sqlConnection?.State != ConnectionState.Closed)
            {
                _sqlConnection.Close();
            }
        }
        public List<Car> GetAllInventory()
        {
            OpenConnection();
            List<Car> inventory = new List<Car>();

            string sql = "select * from inventory";
            using (SqlCommand command = new SqlCommand(sql, _sqlConnection))
            {
                command.CommandType = CommandType.Text;
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                while (reader.Read())
                {
                    inventory.Add(new Car
                    {
                        CarId = (int)reader["CarId"],
                        Color = (string)reader["Color"],
                        Make = (string)reader["Make"],
                        PetName = (string)reader["PetName"]
                    });
                }
                reader.Close();
            }

            return inventory;
        }
        public Car GetCar(int id)
        {
            OpenConnection();
            Car car = new Car();

            string sql = $"select * from inventory where CarId = {id}";
            using (SqlCommand command = new SqlCommand(sql, _sqlConnection))
            {
                command.CommandType = CommandType.Text;
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                while (reader.Read())
                {
                    car = new Car
                    {
                        CarId = (int)reader["CarId"],
                        Color = (string)reader["Color"],
                        Make = (string)reader["Make"],
                        PetName = (string)reader["PetName"]
                    };
                }
                reader.Close();
            }

            return car;
        }
        public void InsertAuto(string color, string make, string petName)
        {
            OpenConnection();
            string sql = $"insert into Inventory (Make, Color, PetName) values ('{make}','{color}','{petName}')";
            using (SqlCommand command = new SqlCommand(sql, _sqlConnection))
            {
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
            }
            CloseConnection();
        }
        public void InsertAuto(Car car)
        {
            OpenConnection();
            //string sql = $"insert into Inventory (Make, Color, PetName) values ('{car.Make}','{car.Color}','{car.PetName}')";
            string sql = "Insert Into Inventory" + "(Make, Color, PetName) values" + "(@Make, @Color, @PetName)";
            using (SqlCommand command = new SqlCommand(sql, _sqlConnection))
            {
                SqlParameter parameter = new SqlParameter
                {
                    ParameterName = "@Make",
                    Value = car.Make,
                    SqlDbType = SqlDbType.Char,
                    Size = 10
                };
                command.Parameters.Add(parameter);

                parameter = new SqlParameter
                {
                    ParameterName = "@Color",
                    Value = car.Color,
                    SqlDbType = SqlDbType.Char,
                    Size = 10
                };
                command.Parameters.Add(parameter);

                parameter = new SqlParameter
                {
                    ParameterName = "@PetName",
                    Value = car.PetName,
                    SqlDbType = SqlDbType.Char,
                    Size = 10
                };
                command.Parameters.Add(parameter);
                command.ExecuteNonQuery();
            }
            CloseConnection();
        }
        public void DeleteCar(int id)
        {
            OpenConnection();
            string sql = $"Delete from Inventory where CarId = '{id}'";
            using (SqlCommand command = new SqlCommand(sql, _sqlConnection))
            {
                try
                {
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Exception error = new Exception("Sorry! That car is on order!", ex);
                    throw error;
                }
            }
            CloseConnection();
        }
        public void UpdateCarPetName(int id, string newPetName)
        {
            OpenConnection();
            string sql = $"Update Inventory Set PetName = '{newPetName}' where CarId = '{id}'";
            using (SqlCommand command = new SqlCommand(sql, _sqlConnection))
            {
                command.ExecuteNonQuery();
            }
            CloseConnection();
        }
        public string LookupPetName(int carId)
        {
            OpenConnection();
            string carPetName;

            using (SqlCommand command = new SqlCommand("GetPetName", _sqlConnection))
            {
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter()
                {
                    ParameterName = "@carId",
                    SqlDbType = SqlDbType.Int,
                    Value = carId,
                    Direction = ParameterDirection.Input
                };
                command.Parameters.Add(param);

                param = new SqlParameter()
                {
                    ParameterName = "@petName",
                    SqlDbType = SqlDbType.Char,
                    Size = 10,
                    Direction = ParameterDirection.Output
                };
                command.Parameters.Add(param);

                command.ExecuteNonQuery();
                carPetName = (string)command.Parameters["@petName"].Value;
                CloseConnection();
            }

            return carPetName;
        }
        public void ProcessCreditRisk(bool throwEx, int custId)
        {
            OpenConnection();
            string fName;
            string lName;
            var cmdSelect = new SqlCommand($"Select * from Customers where CustId = {custId}", _sqlConnection);
            using (var dataReader = cmdSelect.ExecuteReader())
            {
                if (dataReader.HasRows)
                {
                    dataReader.Read();
                    fName = (string)dataReader["FirstName"];
                    lName = (string)dataReader["LastName"];
                }
                else
                {
                    CloseConnection();
                    return;
                }
            }

            var cmdRemove = new SqlCommand($"Delete from Customers where CustId = {custId}", _sqlConnection);

            var cmdInsert = new SqlCommand($"Insert into CreditRisks" + $"(FirstName, LastName) Values ('{fName}','{lName}')", _sqlConnection);

            SqlTransaction tx = null;
            try
            {
                tx = _sqlConnection.BeginTransaction();
                cmdInsert.Transaction = tx;
                cmdRemove.Transaction = tx;
                cmdInsert.ExecuteNonQuery();
                cmdRemove.ExecuteNonQuery();

                if (throwEx)
                {
                    throw new Exception("Sorry! Database error! Transaction failed...");
                }
                tx.Commit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                tx?.Rollback();
            }
            finally
            {
                CloseConnection();
            }
        }
    }
}
