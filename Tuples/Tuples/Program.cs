﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tuples
{
    struct Point
    {
        public int X;
        public int Y;

        public Point(int XPos, int YPos)
        {
            X = XPos;
            Y = YPos;
        }

        public (int XPos, int YPos) Deconstruct() => (X, Y);
    }

    class Program
    {
        static void Main(string[] args)
        {
            Point p = new Point(7, 5);
            var pointValues = p.Deconstruct();
            Console.WriteLine($"X is: {pointValues.XPos}");
            Console.WriteLine($"Y is: {pointValues.YPos}");

            (string, int, string) values = ("A", 5, "c");
            var values2 = ("A", 5, "c");

            Console.WriteLine($"First item: {values.Item1}");
            Console.WriteLine($"Second: {values.Item2}");
            Console.WriteLine($"Third: {values.Item3}");

            var valuesWithNames = (FirstLetter: "a", TheNumber: 5, SecondLetter: "c");

            Console.WriteLine($"First item: {valuesWithNames.FirstLetter}");
            Console.WriteLine($"Second: {valuesWithNames.Item2}");
            Console.Write($"Third: {valuesWithNames.SecondLetter}");

            Console.WriteLine("=> Inferred Tuple Names");
            var foo = new { Prop1 = "first", Prop2 = "second" };
            var bar = (foo.Prop1, foo.Prop2);

            Console.WriteLine($"{bar.Prop1}; {bar.Prop2};");
            var splitName = SplitNames("Phillip J Japikse");
            Console.WriteLine($"{splitName.first}");
            Console.WriteLine($"{splitName.middle}");
            Console.WriteLine($"{splitName.last}");

            // Discard with tuple
            var (first, _, last) = SplitNames("Phillip J Japikse");
            Console.WriteLine($"{first}");
            Console.WriteLine($"{last}");

            Console.ReadLine();
        }

        static (string first, string middle, string last) SplitNames(string fullName)
        {
            // Do what is needed to split names here
            return ("Phillip", "J", "Japikse");
        }
    }
}
