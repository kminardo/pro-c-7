﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Diagnostics;

namespace MyEBookReader
{
    class Program
    {
        private static string theEbook;

        static void Main(string[] args)
        {
            Console.WriteLine("Downloading Book...");
            GetBook();
            Console.ReadLine();
        }

        static void GetBook()
        {
            WebClient wc = new WebClient();
            wc.DownloadStringCompleted += (s, eArgs) =>
            {
                theEbook = eArgs.Result;
                Console.WriteLine("Download Complete");
                GetStats();
                GetStats();
                GetStats();
                GetStats();
                GetStats();
            };

            wc.DownloadStringAsync(new Uri("http://www.gutenberg.org/cache/epub/10/pg10.txt"));
        }

        static void GetStats()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            string[] words = theEbook.Split(new char[] { ' ', '\u000A', ',', '.', ';', ':', '-', '?', '/' }, StringSplitOptions.RemoveEmptyEntries);

            //string[] tenMostCommon = FindTenMostCommon(words);
            //string longestWord = FindLongestword(words);

            string[] tenMostCommon = null;
            string longestWord = string.Empty;

            Parallel.Invoke(() =>
            {
                tenMostCommon = FindTenMostCommon(words);
            }, () =>
            {
                longestWord = FindLongestword(words);
            });

            StringBuilder bookStats = new StringBuilder("Ten most common words are:\n");
            foreach (string s in tenMostCommon)
            {
                bookStats.AppendLine(s);
            }
            
            bookStats.AppendFormat($"Longest word is : {longestWord}");
            bookStats.AppendLine();

            Console.WriteLine(bookStats.ToString(), "Book Info");
            Console.WriteLine($"Processing time: {sw.ElapsedMilliseconds}");
        }

        private static string FindLongestword(string[] words)
        {
            return (from w in words orderby w.Length descending select w).FirstOrDefault();
        }

        private static string[] FindTenMostCommon(string[] words)
        {
            var frequencyOrder = from word in words
                                 where word.Length > 6
                                 group word by word into g
                                 orderby g.Count() descending
                                 select g.Key;
            string[] commonWords = (frequencyOrder.Take(10)).ToArray();
            return commonWords;
        }
    }
}
