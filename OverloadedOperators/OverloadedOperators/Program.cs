﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverloadedOperators
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Working with Overloaded Operators");

            Point p1 = new Point(100, 100);
            Point p2 = new Point(40, 40);

            Console.WriteLine($"p1 = {p1}");
            Console.WriteLine($"p2 = {p2}");

            Console.WriteLine($"p1 + p2 = {p1 + p2}");
            Console.WriteLine($"p1 - p2 = {p1 - p2}");

            Console.WriteLine($"20 + p1 = {20 + p1}");
            Console.WriteLine($"-20 + p1 = {-20 + p1}");

            Console.WriteLine($"p1 += 20 = {p1 += 20}");
            Console.WriteLine($"++p1 = {++p1}");

            Console.WriteLine($"p1 == p2: {p1 == p2}");
            Console.WriteLine($"p1 != p2: {p1 != p2}");

            Console.WriteLine($"p1 < p2: {p1 < p2}");
            Console.WriteLine($"p1 > p2: {p1 < p2}");

            Console.WriteLine($"p1 <= p2: {p1 <= p2}");
            Console.WriteLine($"p1 >= p2: {p1 >= p2}");

            Console.ReadLine();
        }
    }
}
