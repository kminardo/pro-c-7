﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoLotConsoleApp.EF;
using AutoLotConsoleApp.Models;
using static System.Console;

namespace AutoLotConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("***** Working with Entity Framework *****\n");

            WriteLine("Adding new record... ID: ");
            int carId = AddNewRecord();
            Write(carId + "\n");

            WriteLine("Adding new recordS");
            AddNewRecords(new List<Car> {
                new Car {Make = "VW", Color = "Black", CarNickName = "Zippy" },
                new Car {Make = "Ford", Color = "Rust", CarNickName = "Rusty" },
                new Car {Make = "Saab", Color = "Black", CarNickName = "Mel" },
                new Car {Make = "Yugo", Color = "Yellow", CarNickName = "Clunker" },
                new Car {Make = "BMW", Color = "Black", CarNickName = "Bimmer"},
                new Car {Make = "BMW", Color = "Green", CarNickName = "Hank"}
            });
            
            WriteLine("Select all inventory");
            PrintAllInventory();

            WriteLine("ShortCar inventory");
            PrintAllInventoryAsShortCar();

            WriteLine("LINQ Time");
            FunWithLinq();

            UpdateRecord(2);

            ReadLine();
        }

        private static int AddNewRecord()
        {
            using (var context = new AutoLotEntities())
            {
                try
                {
                    var car = new Car() { Make = "Yugo", Color = "Brown", CarNickName = "Brownie" };
                    context.Cars.Add(car);
                    context.SaveChanges();
                    return car.Id;
                }
                catch (Exception ex)
                {
                    WriteLine(ex.InnerException?.Message);
                    return 0;
                }
            }
        }

        private static void AddNewRecords(IEnumerable<Car> carsToAdd)
        {
            using (var context = new AutoLotEntities())
            {
                context.Database.Log = Console.WriteLine;
                context.Cars.AddRange(carsToAdd);
                context.SaveChanges();
            }
        }

        private static void PrintAllInventory()
        {
            using (var context = new AutoLotEntities())
            {
                foreach (Car c in context.Cars)
                {
                    WriteLine(c);
                }
            }
        }

        private static void PrintAllInventoryAsShortCar()
        {
            using (var context = new AutoLotEntities())
            {
                foreach (ShortCar c in context.Database.SqlQuery(typeof(ShortCar), "select CarId, Make from Inventory"))
                {
                    WriteLine(c);
                }
            }
        }

        private static void FunWithLinq()
        {
            using (var context = new AutoLotEntities())
            {
                foreach (Car c in context.Cars.Where(c => c.Make == "BMW"))
                    WriteLine(c);

                var colorMakes = from item in context.Cars select new { item.Color, item.Make };
                foreach (var item in colorMakes)
                    WriteLine(item);

                var blackCars = from item in context.Cars where item.Color == "Black" select item;
                foreach (var item in blackCars)
                    WriteLine(item);

                WriteLine(context.Cars.Find(5));
            }
        }

        private static void RemoveRecord(int carId)
        {
            using (var context = new AutoLotEntities())
            {
                Car carToDelete = context.Cars.Find(carId);
                if (carToDelete != null)
                {
                    context.Cars.Remove(carToDelete);
                    context.SaveChanges();
                }
            }
        }

        private static void RemoveMultipleRecords(IEnumerable<Car> carsToRemove)
        {
            using (var context = new AutoLotEntities())
            {
                context.Cars.RemoveRange(carsToRemove);
                context.SaveChanges();
            }
        }

        private static void RemoveRecordWithEntityState(int carId)
        {
            using (var context = new AutoLotEntities())
            {
                Car carToDelete = new Car() { Id = carId };
                context.Entry(carToDelete).State = System.Data.Entity.EntityState.Deleted;
                try
                {
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    WriteLine(ex);
                }
            }
        }

        private static void UpdateRecord(int carId)
        {
            using (var context = new AutoLotEntities())
            {
                Car carToUpdate = context.Cars.Find(carId);
                if (carToUpdate != null)
                {
                    WriteLine(context.Entry(carToUpdate).State);
                    carToUpdate.Color = "Blue";
                    WriteLine(context.Entry(carToUpdate).State);
                    context.SaveChanges();
                }
            }
        }
    }
}
