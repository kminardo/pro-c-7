﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LazyObjectInstantiation
{
    class Song
    {
        public string Artist { get; set; }
        public string TrackName { get; set; }
        public double TrackLength { get; set; }
    }

    class AllTracks
    {
        private Song[] allSongs = new Song[10000];

        public AllTracks()
        {
            // Assume we fill the songs here
            Console.WriteLine("Filling the songs!");
        }
    }

    class MediaPlayer
    {
        public void Play() { Console.WriteLine("Play a song"); }
        public void Pause() { Console.WriteLine("Pause song"); }
        public void Stop() { Console.WriteLine("Stop playback"); }

        private Lazy<AllTracks> allSongs = new Lazy<AllTracks>(() =>
        {
            Console.WriteLine("Creating AllTracks obj!");
            return new AllTracks();
        });

        public AllTracks GetAllTracks()
        {
            return allSongs.Value;
        }
    }
}
