﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LazyObjectInstantiation
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Working with Lazy Instantiation *****\n");

            MediaPlayer myPlayer = new MediaPlayer();
            myPlayer.Play();

            MediaPlayer yourPlayer = new MediaPlayer();
            AllTracks yourMusick = yourPlayer.GetAllTracks();

            Console.ReadLine();
        }
    }
}
