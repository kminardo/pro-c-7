﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomInterfaces
{
    abstract class Shape
    {
        public Shape(string name = "No Name") { PetName = name; }
        public string PetName { get; set; }

        public abstract void Draw();

        //public virtual void Draw()
        //{
        //    Console.WriteLine("Inside Shape.Draw()");
        //}
    }
}
