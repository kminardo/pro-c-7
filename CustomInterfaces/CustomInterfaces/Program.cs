﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomInterfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Fun with Interfaces *****\n");

            Hexagon hex = new Hexagon();
            Console.WriteLine($"Points: {hex.Points}");

            Hexagon hex2 = new Hexagon("Peter");
            IPointy itfPt2 = hex2 as IPointy;

            if (itfPt2 != null)
                Console.WriteLine($"Points: {itfPt2.Points}");
            else
                Console.WriteLine("OOPS! Not pointy!");



            Shape[] myShapes = { new Hexagon(), new Circle(), new Triangle("Joe"), new Circle() };

            for (int i = 0; i < myShapes.Length; i++)
            {
                myShapes[i].Draw();
                if (myShapes[i] is IPointy ip)
                    Console.WriteLine($"Points: {ip.Points}");
                else
                    Console.WriteLine($"{myShapes[i].PetName} is not pointy!");

                // Can I draw in 3D?
                if (myShapes[i] is IDraw3D)
                    DrawIn3D((IDraw3D)myShapes[i]);

                Console.WriteLine();
            }

            // Find first pointy shape
            IPointy firstPointyShape = FindFirstPointyShape(myShapes);
            if (firstPointyShape != null)
                Console.WriteLine($"The first item has {firstPointyShape.Points} points and named {((Shape)firstPointyShape).PetName}");

            Console.ReadLine();
        }

        static void DrawIn3D(IDraw3D itf3d)
        {
            Console.WriteLine("=> Drawing 3D Type");
            itf3d.Draw3D();
        }

        static IPointy FindFirstPointyShape(Shape[] shapes)
        {
            foreach (Shape s in shapes)
            {
                if (s is IPointy ip)
                    return ip;
            }
            return null;
        }
    }
}
