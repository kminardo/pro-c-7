﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunWithMethods
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Fun with Methods *****");
            int x = 9, y = 10;
            Console.WriteLine($"Before Call X:{x} Y:{y}");
            Console.WriteLine($"Answer is: {AddAsValue(x, y)}");
            Console.WriteLine($"After Call X:{x} Y:{y}");

            int ans;
            AddAsOutRef(x, y, out ans);
            Console.WriteLine($"Answer is: {ans}");

            int i; string str; bool b;
            FillTheseValues(out i, out str, out b);
            Console.WriteLine($"i: {i}, str: {str}, b: {b}");

            string str1 = "flip";
            string str2 = "flop";
            Console.WriteLine($"Before: {str1}, {str2}");
            SwapStrings(ref str1, ref str2);
            Console.WriteLine($"After: {str1}, {str2}");

            string[] stringArray = { "one", "two", "three" };
            int pos = 1;
            Console.WriteLine("=> Use ref return");
            Console.WriteLine("Before {0}, {1}, {2}", stringArray[0], stringArray[1], stringArray[2]);
            ref var refOutput = ref SampleRefReturn(stringArray, pos);
            refOutput = "new";
            Console.WriteLine("After {0}, {1}, {2}", stringArray[0], stringArray[1], stringArray[2]);

            double average;
            average = CalculateAverage(4.0, 3.2, 5.7, 64.22, 87.2);
            Console.WriteLine("Average of data is {0}", average);

            double[] data = { 4.0, 3.2, 5.7 };
            average = CalculateAverage(data);
            Console.WriteLine("Average of data is {0}", average);

            Console.WriteLine($"Average of data 0: {CalculateAverage(0)}");

            Console.ReadLine();
        }

        private static double CalculateAverage(params double[] values)
        {
            Console.WriteLine("You sent me {0} doubles.", values.Length);

            double sum = 0;
            if (values.Length == 0)
                return sum;

            for (int i = 0; i < values.Length; i++)
                sum += values[i];

            return (sum / values.Length);
        }

        private static ref string SampleRefReturn(string[] strArray, int position)
        {
            return ref strArray[position];
        }

        private static void SwapStrings(ref string s1, ref string s2)
        {
            string tempStr = s1;
            s1 = s2;
            s2 = tempStr;
        }

        private static void FillTheseValues(out int a, out string b, out bool c)
        {
            a = 9;
            b = "Enjoy your string";
            c = true;
        }

        private static void AddAsOutRef(int x, int y, out int ans)
        {
            ans = x + y;
        }

        private static object AddAsValue(int x, int y)
        {
            int ans = x + y;
            x = 10000;
            y = 30000;
            return ans;
        }
    }
}
