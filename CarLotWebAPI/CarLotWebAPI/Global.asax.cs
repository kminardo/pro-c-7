﻿using AutoLotDAL.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using AutoLotDAL.EF;
using System.Data.Entity;

namespace CarLotWebAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            Database.SetInitializer(new MyDataInitializer());
            AutoMapperConfig.Initalize();
        }
    }

    public class AutoMapperConfig
    {
        public static void Initalize()
        {
            Mapper.Initialize(
                cfg =>
                {
                cfg.CreateMap<Inventory, Inventory>().ForMember(x => x.Orders, opt => opt.Ignore());
                });
        }
    }
}
