﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringReaderWriterApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Working with StringReader and StringWriter *****\n");

            using (StringWriter sw = new StringWriter())
            {
                sw.WriteLine("Don't forget mother's day");
                Console.WriteLine("Contents of StringWriter:\n {0}", sw);

                StringBuilder sb = sw.GetStringBuilder();
                sb.Insert(0, "Hey!!  ");
                Console.WriteLine("-> {0}", sb.ToString());
                sb.Remove(0, "Hey!!  ".Length);
                Console.WriteLine("-> {0}", sb.ToString());

                using (StringReader sr = new StringReader(sw.ToString()))
                {
                    string input = null;
                    while ((input = sr.ReadLine()) != null)
                    {
                        Console.WriteLine(input);
                    }
                }
            }

            Console.ReadLine();
        }
    }
}
