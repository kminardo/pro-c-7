﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using AutoLotDAL_Core.EF;
using AutoLotDAL_Core.Repos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using AutoLotAPI_Core.Filters;
using AutoMapper;
using AutoLotDAL_Core.Models;

namespace AutoLotAPI_Core
{
    public class Startup
    {
        private readonly IHostingEnvironment _env;
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            _env = env;
            Configuration = configuration;

            Mapper.Initialize(
                cfg =>
                {
                    cfg.CreateMap<Inventory, Inventory>().ForMember(x => x.Orders, opt => opt.Ignore());
                });
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(config => { config.Filters.Add(new AutoLotExceptionFilter(_env)); }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddDbContextPool<AutoLotContext>(
                options => options.UseSqlServer(Configuration.GetConnectionString("AutoLot"),
                o => o.EnableRetryOnFailure())
                .ConfigureWarnings(w => w.Throw(RelationalEventId.QueryClientEvaluationWarning)));

            services.AddScoped<IInventoryRepo, InventoryRepo>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
