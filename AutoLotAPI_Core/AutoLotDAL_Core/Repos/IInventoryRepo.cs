﻿using System.Collections.Generic;
using AutoLotDAL_Core.Models;

namespace AutoLotDAL_Core.Repos
{
    public interface IInventoryRepo : IRepo<Inventory>
    {
        List<Inventory> Search(string stringSearch);
        List<Inventory> GetPinkCars();
        List<Inventory> GetRelatedData();
    }
}
