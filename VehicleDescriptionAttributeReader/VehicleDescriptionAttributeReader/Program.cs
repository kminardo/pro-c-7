﻿using AttributedCarLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleDescriptionAttributeReader
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Value of VehicleDescriptionAttribute *****\n");
            ReflectOnAttributesUsingEarlyBinding();
            Console.ReadLine();
        }

        private static void ReflectOnAttributesUsingEarlyBinding()
        {
            Type t = typeof(Winnebago);

            object[] customAttr = t.GetCustomAttributes(false);

            foreach(VehicleDescriptionAttribute v in customAttr)
                Console.WriteLine("=> {0}\n", v.Description);
        }
    }
}
