﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomEnumerator
{
    public class Garage
    {
        private Car[] carArray = new Car[4];

        public Garage()
        {
            carArray[0] = new Car("Tom", 30);
            carArray[1] = new Car("Andy", 55);
            carArray[2] = new Car("Ron", 30);
            carArray[3] = new Car("Leslie", 30);
        }

        public IEnumerator GetEnumerator() => carArray.GetEnumerator();
    }
}