﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalFunctions
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Add(10,12));
            Console.WriteLine(AddWrapper(10, 12));

            Console.ReadLine();
        }

        static int AddWrapper (int x, int y)
        {
            return Add();

            int Add()
            {
                return x + y;
            }
        }

        static int Add(int x, int y)
        {
            // Do some validation here
            return x + y;
        }
    }
}
