﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomConversions
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Working with Custom Conversions *****");

            Rectangle r = new Rectangle(15, 5);
            Console.WriteLine(r);
            r.Draw();

            Square s = (Square)r;
            Console.WriteLine(s);
            s.Draw();

            DrawSquare((Square)r);

            // Int to square
            Square s2 = (Square)90;

            int side = (int)s2;
            Console.WriteLine("Side length of s2 = {0}", side);

            Square s3 = new Square { Length = 83 };
            Rectangle r2 = s3;
            Console.WriteLine(r2);

            Console.ReadLine();
        }

        static void DrawSquare(Square sq)
        {
            Console.WriteLine(sq);
            sq.Draw();
        }
    }
}
